# Title: Article preview component

Tech stack: Svelte & Vite

Deployed project: https://saad-shaikh-article-preview-component.netlify.app/

## Main tasks:
- Created 2 different share options pop-up via JavaScript for mobile and desktop

## Desktop view:
![Desktop view](design/desktop-design.jpg) 

## Mobile view:
![Mobile view](design/mobile-design.jpg) 
